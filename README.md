# Projet Mécanique 2021

## Sommaire

- [Introduction](./Introduction.md)
- [Partie I - Observer](./Partie I - Observer.md)
    - [Mise à disposition de la vidéo](./Partie I - Observer.md#mise-%C3%A0-disposition-de-la-vid%C3%A9o)
    - [Trajectoire du mouvement](./Partie I - Observer.md#trajectoire-du-mouvement)
    - [Evolution temporelle du mouvement](./Partie I - Observer.md#evolution-temporelle-du-mouvement)
    - [Analyse qualitative](./Partie I - Observer.md#analyse-qualitative)
- [Partie II - Modéliser](./Partie II - Modéliser.md)
    - [Présentation de la situation physique](./Partie II - Modéliser.md#pr%C3%A9sentation-de-la-situation-physique)
    - [Modélisation théorique : Application des lois de Newton pour obtenir les équations horaires théoriques](./Partie II - Modéliser.md#mod%C3%A9lisation-th%C3%A9orique-application-des-lois-de-newton-pour-obtenir-les-%C3%A9quations-horaires-th%C3%A9oriques)
    - [Étude énergétique du mouvement](./Partie II - Modéliser.md#%C3%A9tude-%C3%A9nerg%C3%A9tique-du-mouvement)
    - [Vérification des modèles](./Partie II - Modéliser.md#v%C3%A9rification-des-mod%C3%A8les)
    - [Amélioration du modèles - Pour approfondir](./Partie II - Modéliser.md#am%C3%A9lioration-du-mod%C3%A8les-pour-approfondir)
- [Partie III - Prévoir](./Partie III - Prévoir.md)
    - [Choisir une nouvelle situation expérimentale](./Partie III - Prévoir.md#choisir-une-nouvelle-situation-exp%C3%A9rimentale)
    - [Calculer la trajectoire grâce au modèle mathématique](./Partie III - Prévoir.md#calculer-la-trajectoire-gr%C3%A2ce-au-mod%C3%A8le-math%C3%A9matique)
    - [Réaliser une nouvelle expérience](./Partie III - Prévoir.md#r%C3%A9aliser-une-nouvelle-exp%C3%A9rience)
    - [Confronter la prévision réalisée grâce au modèle, et la réalité de l'expérience](./Partie III - Prévoir.md#confronter-la-pr%C3%A9vision-r%C3%A9alis%C3%A9e-gr%C3%A2ce-au-mod%C3%A8le-et-la-r%C3%A9alit%C3%A9-de-lexp%C3%A9rience)
- [Partie IV - Conclure](./Partie IV - Conclure.md)
- [Partie V - Analyse reflexive](./Partie V - Analyse réflexive.md)

