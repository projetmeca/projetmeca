# ANALYSE REFLEXIVE

Nous avons dans un premier temps rédigé une conclusion générale pour chacun de nous, et nous n'avions pas compris qu'il en fallait un personnel. Nous avons alors décidé de laisser cette conclusion qui représente nos avis collectifs, nous ne voyons pas grand-chose à compléter personnellement et elle nous semble assez complète. C'est donc dans cette dernière partie que nous avons rédigé une conclusion générale qui explique nos différends ressentis par rapport à ce projet. Nous allons ainsi parler de notre perception propre du travail réalisé. Des différentes contraintes qu'implique de travailler en binôme, des difficultés rencontrées mais aussi des points positifs et ce que nous as apporté le fait de réaliser un tel projet.

## Conclusion générale

### Choix du projet

Lors du choix de ce projet, nous avions eu beaucoup d'idées en tête, qui pour la plupart, n'ont malheureusement pas aboutis. Dans un premier temps, comme nous faisions tout les deux du tennis, nous étions ainsi partis pour étudier le mouvement d'une balle de tennis lors d'un service. Mais afin de réaliser cette expérience, un problème majeur s'est posé. En effet, il nous fallait avoir la même vitesse initiale, et la même force initiale pour chaque lancer, ce qui était humainement inconcevable et donc impossible sans l'intervention d'une machine. Cette machine, pourtant, existe c'est donc avec espoir que nous sommes allé demander à nos clubs de tennis respectifs si on pouvait en emprunter une pour réaliser notre projet original. Malheureusement aucun d'entre eux n'en possédaient. Nous avons donc abandonné ce projet pour en essayer deux autres, toujours sans succès. Le premier des deux était celui d'étudier le mouvement d'un bouchon de champagne, ou tout autre liquide gazeux, lorsqu'il saute après avoir agité la bouteille. Mais le même problème apparaît, nous n'avions aucun moyen de vérifier la force initiale, c'est donc dans cette optique que nous avions cherché à voir si nous pouvions étudier le mouvement d'une plante hélicoptère. Mais ici, un autre problème qu'on n'avait pas imaginé apparaît. Une plante hélicoptère jeter à la même hauteur plusieurs fois de suite, n'effectuera jamais, à moins d'un miracle, le même mouvement. C'est donc ainsi que nous sommes partis sur une idée que notre professeur a proposé, à savoir, l'étude du mouvement d'une goutte d'eau dans un verre rempli d'huile. 

## L'expérience

Dès que le choix de notre projet a été acté, nous nous sommes ainsi directement attelés à la mise en oeuvre de l'expérience. Pour cela, nous devions obtenir le mouvement de chute de la goutte d'eau dans le verre d'huile, or il nous fallait trouver un moyen de mettre la goutte d'eau directement dans l'huile, car après de multiples essais, la goutte d'eau restait à la surface de l'huile sans jamais se déplacer. Notre groupe ne possédant pas de pipette, nous avons donc dû utiliser une paille, ignorant du fait que réaliser cette expérience dans ces conditions nous causerait plusieurs problèmes. En effet, en utilisant une paille en carton, malgré le nombre colossal de gouttes d'eau que nous mettions dedans, celles-ci restaient bloquées dans la paille, probablement dû aux différentes forces de frottements situées à l'intérieur de la paille, ainsi que de la densité de l'huile et sa composition. Et après de nombreux efforts, nous avons accepté le fait que nous allons au final d'être forcé de bouger la paille un minimum afin d'arriver à faire couler cette fameuse goutte dans l'huile. Sauf que ce mouvement a fini par provoquer une vitesse initiale à la goutte dont on ignorait à ce moment si elle allait fausser ou non la cohérence de nos résultats. Après de nombreuses tentatives infructueuses au fait de supprimer cette vitesse initiale, nous avons finalement decider de garder le tournage dans lequel ce mouvement est le moins important. 

C'est alors qu'en lisant le document pour utiliser le logiciel LatisPro que nous avons compris qu'il fallait étudier le mouvement sur deux axes X et Y. Vu que nous avions déjà perdu beaucoup de temps et avons pris un retard considérable sur les autres groupes, nous avons finalement gardé notre mouvement même si celui-ci n'est rectiligne sur qu'un unique axe Y.

Nous avons aussi fait 2 autres expériences pour répondre à la troisième partie, la première a été fait sur les conseils de notre professeur. Nous avons ainsi augmenté la température de l'huile dans lequel notre bulle d'eau allait effectuer son mouvement. Nous étions curieux de savoir quel impact ce changement de température allait avoir sur le mouvement de notre bulle d'eau. Grâce à nos recherches faites aux préalables, nous avons su qu'augmenter la température de l'huile la faire se dilater, ce qui allait entrainer une diminution sur sa densité. Nous voulions savoir ce que cela représentait concrètement.

La seconde expérience n'était pas obligatoire mais nous voulions savoir ce que le changement d'huile allait provoquer sur notre expérience. Si nos valeurs allaient changer en fonction des différentes marques d'huiles d'olives, si le fait qu'une des deux huiles était bio allait la rendre "différente" de l'autre. Des questions qui nous ont poussé à faire cette expérience même si elle n'a pas été très concluante dans nos questions.

### Latis-Pro et les courbes

Notre utilisation du logiciel a été simple et rapide. Majoritairement grâce aux différentes vidéos clair, concise et facile à comprendre que le professeur nous a mis à disposition. Mais nous notons quand même le fait qu'il faudrait probablement conseiller un logiciel plus récent afin d'éviter de réduire la qualité/le framerate de la vidéo.

### Les calcules 

Cette partie fut la plus compliquée à effectuer. Au fur et à mesure que nous avançions et complétions nos calculs, nous étions de plus en plus déboussolés du fait qu'on n'en voyait pas la fin. Ce fut un travail long et fastidieux, dont nous ne nous attendions pas avec notre sujet. Pour commencer, nous avons vu que notre bulle ne possédait pas la forme d'une goutte normale, il nous a donc fallu trouver et calculer le volume d'une ellipsoïde de révolution afin de déterminer et calculer les différentes forces qu'agissaient sur notre goutte d'eau. Pour s'ajouter à notre malheur et désespoir, nous avons remarqué qu'à de nombreuses reprises nos résultats n'étaient pas du tout cohérents avec ce que nous espérions obtenir. Nous avons pour cela simplifié le sujet, en faisant une première fois une étude en négligeant les forces de frottements et la poussée d'Archimède, puis une seconde étude avec. Nous ne voulions pas rendre un projet avec toutes la partie de calculs faux ou complètement à coté de la plaque. Pour ajouter à cela, bien que travailler en binôme ait été un réel soutien bénéfique et indispensable lors de cette partie pour nous faire perdre le moins de temps possible et nous auto compléter dans nos points faibles et forts, nous avons été victime d'un problème majeur qui est qu'aucun de nous deux n'avait vraiment compris dans son ensemble le chapitre en énergétique. L'étude énergétique a donc été un réel fléau, et nous avons passé un temps considérable pour comprendre comment l'aborder, ainsi que son utilité et son objectif dans le projet.

### Les résultats

Vu qu'un malheur n'arrive jamais seul, nous avons pu constater en vérifiant nos calculs, que la majorité de nos résultats n'étaient pas cohérents avec les courbes expérimentales obtenues. Et nous n'avions alors aucune idée de la provenance de l'erreur. Nous pouvons quand même noter que la semaine de plus qui nous a été accordé fut un réel rayon de lumière et nous a permis de refaire et trouver la source des erreurs de certains résultats, bien que cela reste minoritaire.

### Perception du travail réalisé et ressentis

Nous pouvons globalement dire que nous avons le même ressenti. 

Ce projet mécanique était un travail très intéressant à faire, bien que la contrainte du distanciel reste toujours présente. Il nous a ainsi permis de nous impliquer dans un travail conséquent en groupe et à réaliser majoritairement en autonomie. Cela a ainsi permis de pouvoir amélioré nos connaissances et compétences dans le domaine de la mécanique et énormément dans le domaine énergétique. Le choix libre du sujet du projet à étudier a aussi été un facteur clé pour nous laisser motiver un temps aussi long. Le travail en binôme quant à lui fut très utile afin d'optimiser notre temps de travail, à nous auto-motive et aussi à ce que chacun puisse faire la partie sur laquelle il aurait compris. Ce temps d'ailleurs, qui fut le plus gros problème du projet avec ce distanciel, selon nous. Nous avons eu aussi un problème de motivation car nous ne voyons pas du tout comment aborder un travail aussi long, mais dès que nous avons commencé, nous avons pu surmonter notre peur et avancer sans revoir ceci nous poser problème. Nous avons aussi aimé à réaliser ce travail ainsi que de faire l'expérience dans un cadre qui nous laissait totalement libres. Si nous devions conclure en une phrase, on peut alors dire que:

Ce projet est un énorme "oui" pour des études d'ingénieur et améliorer ses connaissances en physique, malgré de nombreuses contraintes qui nous aura forcé à nous adapter.


### Répartition globale du travail:

Nous nous sommes réparti le travail assez équitablement dans l'ensemble, selon nous. Suite au distanciel, nous n'avons pu nous réunir pour faire l'expérience et c'est donc Maxime qui s'est majoritairement occupé de la mise en place et de la partie 1.

Alexandre s'est occupé majoritairement de la partie 2 avec quelques interventions de Maxime pour la mise en place de certaines courbes et de certains schémas, ainsi que pour la vérification des calculs. 
La partie 3, quand à elle, a été majoritairement faîtes par Maxime, toujours suite aux circonstances des expériences qui ne pouvaient pas être faites en commun. Alexandre a toutefois aidé sur certains calculs ainsi que leur vérification.

Majoritairement, la partie expérimentale a été majoritairement réalisé par Maxime ROQUELLE
et majoritairement, la partie théorique a été majoritairement réalisé par Alexandre AUGUSTE
Pour le reste, nous avons fait en fonction de nous répartir le travail le plus possible.


