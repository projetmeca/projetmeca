## Choisir une nouvelle situation expérimentale et calculer la trajectoire grâce au modèle mathématique

# Première tentative
Nous avons choisi de travailler également dans un référentiel terrestre supposé galiléen. En effet, dans notre vidéo, l'origine du repère devrait être en théorie le centre de la terre et les différents axes devraient suivre en théorie la rotation de la terre. Cette nouvelle vidéo étant très courte, nous pouvons admettre que le principe d'inertie est vérifié dans ce référentiel terrestre. C’est-à-dire qu’un solide soumis à des forces qui se compensent, est animé d’un mouvement de translation rectiligne uniforme dans ce référentiel. On peut ainsi donc appliquer nos différentes lois de Newton qui nous intéressent pour l’étude du mouvement de notre bulle d'eau. 

En revanche, nous avons modifié les conditions initiales. Dans un premier temps, nous avons changé l'huile dans notre expérience:

Nouvelle **huile** utilisée:

<img src="/images/NewHuile.png" width="200" height="300" />

 

**Vidéo de la nouvelle expérience:**


![NewHuile][NewHuile]
(Clique sur le lien pour télécharger la vidéo si elle ne se lance pas.)


[NewHuile]: ./videos/NewHuile.mp4
## Réaliser une nouvelle expérience

Nous avons donc réalisé la nouvelle expérience. Nous espérions remarquer une différence significative de notre mouvement.Nous avons donc étudié ce nouveau mouvement encore avec le logiciel LatisPro. Nous pensions que les propriétés de ces huiles étaient différentes et qu'ainsi nous obtiendrons un mouvement différent mais ce ne fut pas le cas.

<img src="/images/NewHuileP.png" width="500" height="400" />        <img src="/images/NewHuileV.png" width="500" height="400" />

(Clique sur les images pour les visualiser avec meilleurs qualités.)

Après avoir employé la 2ème Loi de Newton sur ce nouveau mouvement, nous avons remarqué que le mouvement était presque similaire. Afin d'obtenir un mouvement différent, et de réaliser pleinement cette partie 3, nous nous sommes donc lancés dans une nouvelle tentative avec de nouvelles conditions initiales.

# Deuxième tentative

Sur conseil du professeur, nous avons augmenté la température de l'huile dans lequel la goutte d'eau effectue son mouvement. Nous nous sommes donc renseignés sur les propriétés de l'huile pour quelles raisons notre mouvement changerait si nous augmentons sa température. 

>Une huile est un corps gras qui est à l'état liquide à température ambiante et qui ne se mélange pas à l'eau. La densité de l’huile n’est pas toujours la même, étant donné qu’il existe beaucoup de types d’huiles. Généralement, la densité relative de la plupart des huiles, qu’elles soient minérales ou végétales, se situe entre 0.840 et 0.960. Si la température augmente, les molécules du fluide s'écartent et la densité diminue. Si la température baisse, c'est l'inverse. Par conséquent, en augmentant la température, la densité de l'huile diminuera. Nous conjecturons donc que le mouvement d'eau goutte d'eau dans de l'huile chauffée sera plus rapide.

**Vidéo de l'expérience:**
 ![vivi][vivi]


[vivi]: ./videos/NewTemp.mp4


>Nous imaginons donc une vitesse qui monte plus vite que l'ancien mouvement dû à la diminution de la densité de l'huile. Ainsi, la position de la goutte atteint plus rapidement l'eau située sous l'huile.

# Etablissement des équations horaires pour vérifier la conjecture.

N'ayant pas le matériel ni le temps requis pour trouver la masse de la goutte de cette nouvelle expérience, nous prendrons la même masse (de la goutte d'eau) que la partie 2.

Notre système qui est une bulle d'eau lors de la vidéo entre en contact avec de l'huile d'olive versée dans un verre de 15cl.
Lors de la chute de la bulle d'eau, nous pouvons remarquer la présence de 3 forces qui s'appliquent sur le système :

- Le poids : $`\vec{P} = m \vec{g}`$ avec $`\lVert\vec{P}\rVert = m_{théorique} \cdot g = 3\cdot 10^{-4} \cdot 9,81 \approx 2,9\cdot 10^{-3} N`$
- La poussée d'Archimède : $`\vec{\Pi_A} = \rho_{déplacé} \cdot V_{déplacé} \cdot \vec{g} = \rho_{huile d'olive} \cdot V_{bulle d'eau} = 9.137\cdot 10^2 \cdot 3\cdot 10^{-7} \cdot 9.81 \approx 2.7\cdot 10^{-3}  N`$
- Les forces de frottements : $`\vec{f_f}= −\lambda \vec{v}`$

>Nous nous rendons compte, grâce à ces formules, que la poussée d'Archimède et potentiellement les forces de frottements changeront comparé à la première expérience. Nous savons que la densité diminue en augmentant, or nous savons également que la densité se calcule par cette formule:
 <img src="/images/formule2.png" width="400" height="200" />

>Or, nous n'avons pas modifié l'eau utilisée dans cette expérience, par conséquent la masse volumique de l'eau est la même que l'ancienne expérience. Donc si la densité baisse, c'est que la masse volumique de l'huile baisse, influençant donc la poussée d'Archimède, ce qui pourrait justifier un mouvement plus rapide.

On cherche $` \lambda `$ pour calculer les forces de frottements exercées sur notre bille. On utilise alors la formule donnée par le [Tableau des Cx linéaires en régime de Stokes](https://fr.wikipedia.org/wiki/Fichier:Tableau_des_cx_linéaires_de_quelques_particules_en_Régime_de_Stokes.png#:~:text=Ce%20Cx%20linéaire%20est%20défini,la%20particule%20ou%20sa%20longueur) qui est :
- $` f_f = Cx \cdot \mu \cdot V \cdot L `$
On en déduit alors que $`\lambda = Cx \cdot \mu \cdot L `$
On [sait](https://en.wikipedia.org/wiki/Viscosity#Other_common_substances) que la viscosité dynamique de l'huile $` \mu est de 5,6\cdot 10^{-2} Pa.s`$ pour une température de $`T=26 \degree C`$. 

>La viscosité détermine la vitesse de mouvement du fluide (par exemple, la vitesse de déplacement d'une cuillère dans un bol: plus le liquide est visqueux, plus le mouvement est lent). Or, la température de notre huile, est bien plus élevée. En effet, afin de chauffer l'huile, nous l'avons chauffée dans une bouilloire, augmentant considérablement sa température. Malheureusement, nous n'avons pas de thermomètre pour voir la température de l'huile. Nous conjecturons que l'huile est environ à 100 degré Celsius. Et nous savons que, contrairement à celle d'un gaz, la viscosité d'un liquide diminue lorsque la température augmente. Par conséquent la viscosité de l'huile, dans cette nouvelle expérience, est bien inférieure à celle de la partie 2, ce qui pourrait également justifier du mouvement plus rapide conjecturé.


En prenant en exemple une vitesse avec un ordre de grandeur de $` 10^0 `$, l'ordre de grandeur des forces de frottements est de 10^{-3} qui sont normalement, comme la poussée d'Archimède, non négligeable dans notre étude.

Les conditions initiales sont à _t_ = 0.

- $` x(0) = 0 `$
- $` y(0) = y_o = 0 `$
- $`On ne travaille pas sur x car sa valeur ne varie pas dans la courbe illustrant la trajectoire de notre vidéo`$
- $`v_y(0) = C_1 `$

**La deuxième loi de Newton s'écrit donc:**

```math
\begin{aligned}
    \sum\vec{F_{ext}}=\frac{d\vec{p}}{dt}\\
\end{aligned}
```

La masse de notre système étant constant, on a :

```math
    \begin{aligned}
        \sum\vec{F_{ext}}&= m\vec{a}\\
        \vec{P} + \vec{\Pi_A} + \vec{f_f} = m_{bulled'eau} \vec{a(t)}\\
    \end{aligned}
```
**En projection sur l'axe descendant, on obtient**

```math
    \begin{aligned}
        m_{bulled'eau}\cdot \vec{g} + \rho_{huile} \cdot V_{bulled'eau} \cdot \vec{g} + −\lambda \vec{v(t)} &= m_{bulled'eau} \vec{a(t)}\\
        -g + \rho_{huile} \cdot V_{bulled'eau} \cdot g \cdot 1/m_{bulled'eau} + −\lambda \cdot v(t)\cdot 1/m_{bulled'eau} &= \vec{a(t)}\\
    \end{aligned}
```

Après simplification, on remarque qu'on a une équation du premier ordre sur la vitesse, telle que :

```math
    \begin{aligned}
        \frac{dv}{dt} + \frac{k}{m_{bulled'eau}}\cdot v(t)= g(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}})\\
    \end{aligned}
```
On résout cette équation différentielle. On sait que la solution de cette équation différentielle du premier ordre à coefficients constants est donc :

```math
    \begin{aligned}
        v(t) = Ke^{-\frac{k}{m}\cdot t} + \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}})\\      
    \end{aligned}
```

A l'instant initial, notre bulle d'eau a une vitesse nulle, donc v(0)=0. On peut en déduire la constante d'intégration K:

```math
    \begin{aligned}
        K = \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}})\\       
    \end{aligned}
```

On a donc :

```math
    \begin{aligned}
       v(t) = \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}})\cdot e^{-\frac{k}{m}\cdot t} + \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}})\\
       v(t) =  \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}}) \cdot (1 - e^{-\frac{k}{m}\cdot t})\\
    \end{aligned}
```

Pour trouver l'accélération en fonction de v(t), on intègre :

```math
    \begin{aligned}
       v(t) =  \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}}) \cdot (1 - e^{-\frac{k}{m}\cdot t})\\
       a(t)= -\frac{m}{k}*\frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}})\cdot e^{-\frac{k}{m}\cdot t} + \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}}*t + C
    \end{aligned}
```

Et ensuite pour trouver la position en fonction de v(t), on dérive :
```math
    \begin{aligned}
       v(t) =  \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}}) \cdot (1 - e^{-\frac{k}{m}\cdot t})\\
       x(t)= -\frac{k}{m}*\frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}})\cdot e^{-\frac{k}{m}\cdot t}
    \end{aligned}
```
>Ayant les mêmes forces que la partie 2, ces équations sont similaires. En revanche, numériquement, ces équations sont différentes puisque la densité, la masse volumique et la viscosité de l'huile sont différentes.

## Confronter la prévision réalisée grâce au modèle

**Conclusion:**

>Dans cette nouvelle expérience, nous avons augmenté la température de l'huile. De ce fait, après avoir fait plusieurs recherches, nous avons appris qu'ainsi, la densité de cette huile allait diminuer. Or, lorsque la densité baisse, la masse volumique diminue également puisque la masse volumique de l'eau est constante. Et nous savons que:

$`\vec{\Pi_A} = \rho_{déplacé} \cdot V_{déplacé} \cdot \vec{g} = \rho_{huile d'olive} \cdot V_{bulle d'eau} = 9.137\cdot 10^2 \cdot 3\cdot 10^{-7} \cdot 9.81 \approx 2.7\cdot 10^{-3}  N`$

>Donc si la masse volumique de l'huile baisse, alors la poussée d'Archimède est de moins en moins importante.
 Nous avons également appris que, contrairement à celle d'un gaz, la viscosité d'un liquide diminue lorsque la température augmente. Or,

 $` f_f = Cx \cdot \mu \cdot V \cdot L `$
On en déduit alors que $`\lambda = Cx \cdot \mu \cdot L `$

>On en déduit donc que \mu va baisser et donc \lambda également. Ce qui va rendre les forces de frottements de moins en moins importantes.
Les 2 forces s'opposant à la trajectoire du mouvement sont moins importantes, on conjecture donc que le mouvement sera plus rapide. En effet, on suppose que la goutte atteint bien plus rapidement l'eau située sous l'huile.

**Conjecture de la vitesse et de la position de la goutte du nouveau mouvement!**

<img src="/images/conjecture.png" width="500" height="300" />  <img src="/images/conjectureV.png" width="500" height="300" />

# La réalité de l'expérience

Après avoir conjecturé ces courbes, nous allons étudier et pointer le mouvement de la vidéo pour vérifier notre hypothèse:

**Graphique représentant la position Y de la goutte en fonction du temps**

<img src="/images/huilechaudey.png" width="900" height="700" /> 

>On remarque que la goutte traverse toute la hauteur de l'huile en à peine **1s**. **L'hypothèse parait vérifiée.**

**Graphique représentant la vitesse de la goutte en fonction du temps**

<img src="/images/huilechaudeV.png" width="900" height="700" />

>On remarque que la vitesse atteint très rapidement un pic comme nous l'avions prévu. **L'hypothèse parait encore plus vérifiée.**

**Graphique représentant l'accelération de la goutte en fonction du temps**

<img src="/images/huilechaudeA.png" width="900" height="700" />

>On remarque que l'accelération augmente très rapidement puis diminue (moment où la goutte atteint l'eau) comme nous l'avions prévu. **L'hypothèse est vérifiée.**






