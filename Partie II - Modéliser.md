## Présentation de la situation physique
On a choisi de travailler pour ce projet dans un référentiel terrestre supposé galiléen. En effet, dans notre vidéo, l'origine du repère devrait être en théorie le centre de la terre et les différents axes devraient suivre en théorie la rotation de la terre. Notre vidéo étant très courte, nous pouvons admettre que le principe d'inertie est vérifié dans ce référentiel terrestre. C’est-à-dire qu’un solide soumis à des forces qui se compensent, est animé d’un mouvement de translation rectiligne uniforme dans ce référentiel. On peut ainsi donc appliquer nos différentes lois de Newton qui nous intéressent pour l’étude du mouvement de notre bulle d'eau. Voici un schéma du mouvement:

 ![schema]

Notre système étudié est une bulle composée d'eau. On peut observer sur la vidéo que notre bulle est de forme ellipsoïde.
Or on sait que le volume d'une ellipsoïde est :

- $` V_{ellipsoide} = \frac{4}{3} \cdot \Pi \cdot a \cdot b \cdot c `$

 ![data]

D'après le schéma, on a :

- a = A/2 = $`5\cdot 10^{-3}`$m
- b = B/2 = $`3,75\cdot 10^{-3}`$m
- c = C/2 = $`3,75\cdot 10^{-3}`$m

On peut donc facilement en déduire que le volume de notre bulle d'eau est de :

- $` V_{bulle d'eau} = \frac{4}{3} \cdot \Pi \cdot 5\cdot 10^{-3} \cdot 3,75\cdot 10^{-3} \cdot 3,75\cdot 10^{-3} \approx 3\cdot 10^{-7}m^3`$

On sait que la masse volumique de l’eau est :
- $` \rho_{eau} = 1000 kg/m^3 `$ 

Donc on peut en déduire que la masse de notre bulle d'eau est :
- $` m_{bulled'eau} = V_{bulled'eau} \cdot \rho_{eau} = 3\cdot 10^{-7} \cdot 1000 \approx 3\cdot 10^{-4} kg`$

Notre système qui est une bulle d'eau lors de la vidéo entre en contact avec de l'huile d'olive versée dans un verre de 15cl.
Lors de la chute de la bulle d'eau, nous pouvons remarquer la présence de 3 forces qui s'appliquent sur le système :

- Le poids : $`\vec{P} = m \vec{g}`$ avec $`\lVert\vec{P}\rVert = m_{théorique} \cdot g = 3\cdot 10^{-4} \cdot 9,81 \approx 2,9\cdot 10^{-3} N`$
- La poussée d'Archimède : $`\vec{\Pi_A} = \rho_{déplacé} \cdot V_{déplacé} \cdot \vec{g} = \rho_{huile d'olive} \cdot V_{bulle d'eau} = 9.137\cdot 10^2 \cdot 3\cdot 10^{-7} \cdot 9.81 \approx 2.7\cdot 10^{-3}  N`$
- Les forces de frottements : $`\vec{f_f}= −\lambda \vec{v}`$

[schema]: ./images/schema.png
[data]: ./images/data.png
[formule]: ./images/formule.png
[energie]: ./images/energieSansFrot.png

On cherche $` \lambda `$ pour calculer les forces de frottements exercées sur notre bille. On utilise alors la formule donnée par le [Tableau des Cx linéaires en régime de Stokes](https://fr.wikipedia.org/wiki/Fichier:Tableau_des_cx_linéaires_de_quelques_particules_en_Régime_de_Stokes.png#:~:text=Ce%20Cx%20linéaire%20est%20défini,la%20particule%20ou%20sa%20longueur) qui est :
- $` f_f = Cx \cdot \mu \cdot V \cdot L `$
On en déduit alors que $`\lambda = Cx \cdot \mu \cdot L `$ 

On [sait](https://en.wikipedia.org/wiki/Viscosity#Other_common_substances) que la viscosité dynamique de l'huile $` \mu est de 5,6\cdot 10^{-2} Pa.s`$ pour une température de $`T=26 \degree C`$. (La viscosité détermine la vitesse de mouvement du fluide (par exemple, la vitesse de déplacement d'une cuillère dans un bol: plus le liquide est visqueux, plus le mouvement est lent)).

On sait que la longueur caractéristique L est de $` 1\cdot 10^-2 m `$ (équivalent à A d'après notre schéma).
On calcule Cx d'après la formule :
![formule][formule]
- Avec $` \lambda = \frac{B}{A} = \frac{7.5\cdot 10^{-1}}{1\cdot 10^{-2}} = 7.5\cdot 10^1 `$

On remplace les valeurs et on calcule. On obtient alors le resultat $` Cx \approx 9 `$

Donc $` \lambda = Cx \cdot \mu \cdot L = 9 \cdot 5,6\cdot 10^{-2} \cdot 1\cdot 10^-2 \approx 5\cdot 10^-3 `$

En prenant en exemple une vitesse avec un ordre de grandeur de $` 10^0 `$, l'ordre de grandeur des forces de frottements est de 10^{-3} qui sont normalement, comme la poussée d'Archimède, non négligeable dans notre étude.

Les conditions initiales sont à _t_ = 0.

- $` x(0) = 0 `$
- $` y(0) = y_o = 0 `$
- On ne travaille pas sur x car sa valeur ne varie pas dans la courbe illustrant la trajectoire de notre vidéo
- $`v_y(0) = C_1 `$



## Modélisation théorique : Application des lois de Newton pour obtenir les équations horaires théoriques

Le système {bulle d'eau} de masse m = $` 3\cdot 10^{-4}`$ kg est soumis dans le référentiel terrestre supposé galiléen à l'action du Poids $`\vec{P}`$,  La poussée d'Archimède : $`\vec{\Pi_A}`$ et les forces de frottements $`\vec{f_f}`$.

**La deuxième loi de Newton s'écrit donc:**

```math
\begin{aligned}
    \sum\vec{F_{ext}}=\frac{d\vec{p}}{dt}\\
\end{aligned}
```

La masse de notre système étant constant, on a :

```math
    \begin{aligned}
        \sum\vec{F_{ext}}&= m\vec{a}\\
        \vec{P} + \vec{\Pi_A} + \vec{f_f} = m_{bulled'eau} \vec{a(t)}\\
    \end{aligned}
```
Normalement, on ne doit pas négliger les forces de frottements et la poussée d'Archimède, mais le sujet demande plus tard de faire l'étude avec "les forces jusqu'alors négligées(poussée d'Archimède, frottements...)". On fait une première étude en négligeant ces forces. On a alors : 

On ne travaillera que sur l'axe des ordonnées car x ne varie pas lors de notre vidéo
```math
    \begin{aligned}
        \sum\vec{F_{ext}}&= m\vec{a}\\
        \vec{P} = m_{bulled'eau} \vec{a(t)}\\
        m \vec{g} &= m\vec{a}\\
        -g\vec{e_y} &= \vec{a_x} + \vec{a_y}\\
    \end{aligned}
```


**En projection sur l'unique axe du repère :**

```math
\begin{aligned}
    &\begin{cases}
        a_y(t) = -g\\
    \end{cases}\\
\end{aligned}
```

Par intégration, on a :

```math
\begin{aligned}
    &\begin{cases}
        v_y(t) = -g\cdot t + C_1\\
    \end{cases}\\
\end{aligned}
```

**Conditions initiales sur la vitesse**

D'après notre vidéo
```math
\begin{aligned}
    &\begin{cases}
        v_{0} = 0\\
    \end{cases}\\
\end{aligned}
```
D'après la seconde loi de Newton:

```math
\begin{aligned}
    &\begin{cases}
        v_y(0)(t) = C_1\\
    \end{cases}\\
\end{aligned}
```
En sachant que $`v_{y0} = v_y(0)`$ d'où $`C_1 = 0`$

```math
\begin{aligned}
    &\begin{cases}
        v_y(t) = -g\cdot t\\
    \end{cases}\\
\end{aligned}
```

Par intégration, on a :

```math
\begin{aligned}
    &\begin{cases}
        y(t) = -\frac{1}{2}g\cdot t^2 + C_2\\
    \end{cases}\\
\end{aligned}
```

**Conditions initiales sur la position**

D'après la vidéo:

```math
\begin{aligned}
    &\begin{cases}
        y_0 = 0\\
    \end{cases}\\
\end{aligned}
```

D'après la seconde loi de Newton:

```math
\begin{aligned}
    &\begin{cases}
        y(0) = C_2\\
    \end{cases}\\
\end{aligned}
```

En sachant que $`y_0 = y(0)`$ d'où $`C_2 = 0`$

**Equations horaires du mouvement**

```math
\begin{aligned}
    &\begin{cases}
        a_y(t) = -g\\
    \end{cases}\\
\end{aligned}
```

```math
\begin{aligned}
    &\begin{cases}
        v_y(t) = -g\cdot t\\
    \end{cases}\\
\end{aligned}
```

```math
\begin{aligned}
    &\begin{cases}
        y(t) = -\frac{1}{2}g\cdot t^2\\
    \end{cases}\\
\end{aligned}
```
**Equation de la trajectoire**

On isole t pour le réinjecter dans notre équation.
Or on ne travaille que sur un seul axe, il n'existe donc pas d'équation de la trajectoire. 
On remarque que la bulle d'eau est animée d'un mouvement rectiligne uniformément accéléré (le vecteur accélération ne change pas) qui se déplace le long de l'axe des abscisses tel que x = 0.
## Étude énergétique du mouvement

Pour l'étude énergétique, nous pouvons observer trois forces en action sur le système, la poussée d'archimède et le poids qui sont des forces conservatives, ainsi que les frottements qui est, quand à lui, une force non conservative.

On a dans notre système 3 forces, le poids $`\vec{P}`$, la poussée d'Archimède $`\vec{\Pi_A}`$, la force de frottement $`\vec{f_f}`$. On repère une seule phases dans le mouvement qui est unqieuemnt descendant. On peut donc en déduire le travail des différentes forces lors de la descente: $`\vec{P}`$ moteur, $`\vec{\Pi_A}`$ résistant et $`\vec{f_f}`$ résistant.

Pour travailler, nous posons deux points A et B tel que A est la position initiale de notre bulle d'eau et B sa position lors du mouvement effectué.

On réalise alors un théorème de l'énergie cinétique (TEC) entre les points A et B.

```math
\begin{aligned}
    \Delta_{A \to B} Ec = Ec_B - Ec_A = W_{A \to B}(\vec{P})
\end{aligned}
```

La bulle d'eau possède une vitesse initiale nulle notée $`v_0 = 0`$ donc $`E_{c_A} = \frac{1}{2} m v_0^2 = 0 `$. La bulle d'eau atteint le point B dans l'huile à la vitesse $`\vec{v}`$ de norme $`v`$. On peut donc écrire la variation de l'énergie cinétique :

```math
\begin{aligned}
    \Delta_{A \to B} Ec = Ec_B - Ec_A = \frac{1}{2} m v^2 - \frac{1}{2} m v_0^2 = \frac{1}{2} m v^2
\end{aligned}
```
Comme dit précèdemment, nous allons négliger la poussée d'Archimède et les forces de frottements, nos résultats ne seront donc pas forcément cohérents(On refera normalement une étude avec lors de [l'amélioration du modèle](#am%C3%A9lioration-du-mod%C3%A8les-pour-approfondir)). On obtient alors :

```math
\begin{aligned}
    \delta W (\vec{P}) = - m g \vec{e_y} . d \overrightarrow{OM} = - mg dy
\end{aligned}
```

On applique et développe pour obtenir un résultat :

```math
\begin{aligned}
        W_{A \to B} (\vec{P}) &= \int^{y_B}_{y_A} \delta W \space dy\\ 
        W_{A \to B} (\vec{P}) &= - m g [ y ]^{y_B}_{y_A}\\ 
        W_{A \to B} (\vec{P}) &= - m g (y_B - y_A)\\
        W_{A \to B} (\vec{P}) &= m g (y_B - y_B)\\
        W_{A \to B} (\vec{P}) &= m g (y_0 - y)
\end{aligned}
```

On peut remarquer que le travail du poids est forcément strictement supérieur à 0, en effet nos valeurs sur l'axe y sont forcément négatives car on base notre point de départ à 0. Donc $` mg (y_0 - y) > 0 `$, c'est donc cohérent avec ce qu'on avait marqué précèdemment, on a un bien obtenu un travail moteur. On obtient donc finalement :

```math
\begin{aligned}
        \Delta_{A \to B} E_c &= W_{A \to B} (\vec{P})\\
        \frac{1}{2} m v^2 &= m g (y_0 - y)\\
        v^2 &= 2 g (y_0 - y)\\
        v &= \sqrt{2 g (y_0 - y)}
\end{aligned}
```

En posant numériquement que $`y = y_0`$, on a : 

$`v = \lvert v_0 \rvert = v_0 m.s^{-1}`$.

On calcule alors les différentes énergies avec les formules du cours

$`Ec = \frac{1}{2} m v^2`$

$`E_{pp} = m g y`$

$`E_m = E_c + E_{pp}`$

On trouve alors :

```math
\begin{aligned}
        Ec &= \frac{1}{2} m v^2\\
        Ec &= \frac{1}{2} m \cdot \lvert 2 g (y_0 - y) \rvert\\
        Ec &= m \lvert g (y_0 - y) \rvert\\
\end{aligned}
```

![energie][energie]

Le résultat peut donc être $` m ( g (y_0 - y) ) g (y_0 - y) \ge 0 `$. On multipliera ce résultat par -1 pour que ce soit cohérent si  $` 0 \ge g (y_0 - y))`$

On rappelle que Ec = m ( g ( y_0 - y )) g (y_0 - y) \ge 0

On rappelle que E_{pp} = m g y

On calcule donc $`E_m`$ : 

```math
\begin{aligned}
    E_m &= E_c + E_{pp}\\
    E_m &= m ( g (y_0 - y)) + m g y\\
    E_m &= m g ( y_0 - y) + m g y\\
    E_m &= y_0 - y + y\\
    E_m &= y_0\\
\end{aligned}
```
--------------------mettre courbe-----------------

## Vérification des modèles

 <img src="/images/Y(t).png" width="500" height="300"/>

 <img src="/images/V_y(t).png" width="500" height="300"/>

 <img src="/images/A_y(t).png" width="500" height="300"/>

<img src="/images/aupif.png" width="500" height="300"/>

Nous pouvons voir que les courbes ne sont pas cohérentes mais présente toutes fois certaines ressemblances.



## Amélioration du modèles - Pour approfondir
**La deuxième loi de Newton s'écrit donc:**

```math
\begin{aligned}
    \sum\vec{F_{ext}}=\frac{d\vec{p}}{dt}\\
\end{aligned}
```

La masse de notre système étant constant, on a :

```math
    \begin{aligned}
        \sum\vec{F_{ext}}&= m\vec{a}\\
        \vec{P} + \vec{\Pi_A} + \vec{f_f} = m_{bulled'eau} \vec{a(t)}\\
    \end{aligned}
```
**En projection sur l'axe descendant, on obtient**

```math
    \begin{aligned}
        m_{bulled'eau}\cdot \vec{g} + \rho_{huile} \cdot V_{bulled'eau} \cdot \vec{g} + −\lambda \vec{v(t)} &= m_{bulled'eau} \vec{a(t)}\\
        -g + \rho_{huile} \cdot V_{bulled'eau} \cdot g \cdot 1/m_{bulled'eau} + −\lambda \cdot v(t)\cdot 1/m_{bulled'eau} &= \vec{a(t)}\\
    \end{aligned}
```

Après simplification, on remarque qu'on a une équation du premier ordre sur la vitesse, telle que :

```math
    \begin{aligned}
        \frac{dv}{dt} + \frac{k}{m_{bulled'eau}}\cdot v(t)= g(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}})\\
    \end{aligned}
```
On résout cette équation différentielle. On sait que la solution de cette équation différentielle du premier ordre à coefficients constants est donc :

```math
    \begin{aligned}
        v(t) = Ke^{-\frac{k}{m}\cdot t} + \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}})\\      
    \end{aligned}
```

A l'instant initial, notre bulle d'eau a une vitesse nulle, donc v(0)=0. On peut en déduire la constante d'intégration K:

```math
    \begin{aligned}
        K = \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}})\\       
    \end{aligned}
```

On a donc :

```math
    \begin{aligned}
       v(t) = \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}})\cdot e^{-\frac{k}{m}\cdot t} + \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}})\\
       v(t) =  \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}}) \cdot (1 - e^{-\frac{k}{m}\cdot t})\\
    \end{aligned}
```

Pour trouver l'accélération en fonction de v(t), on intègre :

```math
    \begin{aligned}
       v(t) =  \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}}) \cdot (1 - e^{-\frac{k}{m}\cdot t})\\
       a(t)= -\frac{m}{k}*\frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}})\cdot e^{-\frac{k}{m}\cdot t} + \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}}*t + C
    \end{aligned}
```

Et ensuite pour trouver la position en fonction de v(t), on dérive :
```math
    \begin{aligned}
       v(t) =  \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}}) \cdot (1 - e^{-\frac{k}{m}\cdot t})\\
       x(t)= -\frac{k}{m}*\frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}})\cdot e^{-\frac{k}{m}\cdot t}
    \end{aligned}
```

On obtient donc ** les équations horaires du mouvement **

```math
\begin{aligned}
    &\begin{cases}
        a_y(t) = -\frac{m}{k}*\frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}})\cdot e^{-\frac{k}{m}\cdot t} + \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}}*t + C
    \end{cases}\\
\end{aligned}
```

```math
\begin{aligned}
    &\begin{cases}
        v_y(t) = \frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}}) \cdot (1 - e^{-\frac{k}{m}\cdot t})
    \end{cases}\\
\end{aligned}
```

```math
\begin{aligned}
    &\begin{cases}
        y(t) = -\frac{k}{m}*\frac{g\cdot m_{bulled'eau}}{k}(1 - \frac{\rho_{huile}V_{bulled'eau}}{m_{bulled'eau}})\cdot e^{-\frac{k}{m}\cdot t}\\
    \end{cases}\\
\end{aligned}

Les courbes obtenues ne donnent pas de résultats cohérent, ni exploitables.

### Pour la partie énergétique

Nous allons reprendre ce qu'on a fait, puis nous allons ajouter les forces de frottements.

On calcule le travail pour les forces de frottements :

```math
\begin{aligned}
    \delta W (\vec{f_f}) = \vec{f_f} . d \overrightarrow{OM}
\end{aligned}
```

Puis :

```math
\begin{aligned}
        W_{A \to B} (\vec{f_f}) &= \int^{B}_{A} \delta W (\vec{f_f}) \space dl\\
        W_{A \to B} (\vec{f_f}) &= - \lambda v^2 \cdot d\\
\end{aligned}
```

Avec $`d`$ la distance entre $`A`$ et $`B`$ :
```math
\begin{aligned}   
        d &= y_0 - y\\     
\end{aligned}
```

On applique alors le théorème d'énergie cinétique :

En appliquant le TEC:

```math
\begin{aligned}
        \Delta_{A \to B} Ec = Ec_M - Ec_B &= W_{A \to B}(\vec{P}) + W_{A \to B}(\vec{f_f})\\
        \frac{1}{2} m v^2 &= m g (y_0 - y) - \lambda v^2 \cdot d\\
        \frac{1}{2} m v^2 &= m g (y_0 - y) - \lambda v^2 \cdot d\\
        m v^2 + 2 \lambda v^2 \cdot d &= 2 m g (y_0 - y)\\
        v^2 \cdot (m + 2 \lambda d) &= m \cdot (2 g (y_0 - y))\\
        v^2 &= m \cdot \frac{2 g (y_0 - y)}{m + 2 \lambda d}\\
        v &= \sqrt{m \cdot \frac{2 g (y_0 - y)}{m + 2 \lambda d}}\\
\end{aligned}
```

Les courbes obtenues ne donnent pas de résultats cohérents, ni exploitables.
