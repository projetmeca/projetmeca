## Mise à disposition de la vidéo

![video]

## Trajectoire du mouvement

Dans ce projet, nous étudions le mouvement de la chute d'une bulle d'eau dans un verre rempli majoritairement d'huile d'olive. Pour étudier le système qui est la bulle d'eau, nous travaillerons avec un référentiel **terrestre supposé galiléen** avec comme support un repère **orthonormé ($`O`$;$`\vec{e_x}`$;$`\vec{e_y}`$)**

 ![trajectoire]

Remarque: 

> Nous constatons un mouvement rectiligne. 

On fera apparaître sur cette trajectoire des vecteurs vitesse et des vecteurs accélération à des instants judicieusement choisis. On expliquera rapidement pourquoi ces vecteurs sont cohérents avec la trajectoire: 

![trajectoiree]

>Etant un mouvement rectiligne, on remarque que la vitesse suit le sens de la trajectoire à n'importe quel point. 

>En revanche, l'accélération diffère selon les points. L'accélération se voit dans le sens de la trajectoire lorsque les points s'éloignent, alors qu'elle est du sens opposée lorsque les points se resserrent.
En effet, lorsque le mouvement accélère, l'accélération est positive, mais si celui-ci ralentit, l'accélération est négative. Cela concorde avec le graphique obtenu.

[trajectoire]: ./images/mouvement.png
[trajectoiree]: ./images/mouvement+vecteur.png
[trajectoireee]: ./images/mouvementY.png
[trajectoireeee]: ./images/Y(t).png
[trajectoireeeee]: ./images/V_y(t).png
[trajectoireeeeee]: ./images/A_y(t).png
[video]: ./videos/trueVideo.mp4


## Evolution temporelle du mouvement et analyse qualitative

 Les évolutions temporelles du mouvement sur les deux directions de l’espace sont attendues  

 On fera une analyse qualitative des courbes (position, vitesse, accélération) obtenues pour montrer qu’elles sont cohérentes avec le mouvement effectivement observé pour le système. On pourra notamment discuter : 

**Graphique représentant la position sur Y en fonction du temps.**

 ![trajectoireeee]

**Commentaire:**
 >La courbe débute à la position **0** du repère (choisi comme centre 0 de celui-ci au préalable).

 >Elle stagne de **0s** à **1.8s**. 
 
 >Puis la courbe ressemble à une pente descendante jusqu'a t= **4.8s**.
 
 >Enfin, elle se stabilise jusqu'à atteindre environ **38mm**.

 >On conclut, que la première partie de la courbe correspond en effet, au moment où la goutte est accrochée à la paille et n'est pas encore entrée en contacte avec l'huile (ce que justifie son immobilisation). Par la suite, la pente correspond au moment du mouvement où la goutte entame son entrée dans l'huile jusqu'à son contacte avec l'eau. Enfin, à **38mm**, la goutte est immobile au contacte de l'eau, située en dessous de l'huile (on determinera ce phénomène plus tard).




 **Graphique représentant la vitesse sur Y en fonction du temps.**

 ![trajectoireeeee]
 
 
**Commentaire:** (La courbe représentant la vitesse de la goutte en fonction du temps est ici représentée en rose.)
 >La courbe débute à la position **0** du repère (choisi comme centre 0 de celui-ci au préalable).

 >Elle stagne de **0s** à **1.6s**. 
 
 >Puis la courbe est décroissante jusqu'à atteindre un minimum à t= **3s**.
 
 >Alors, celle-ci croit jusqu'à re-être nulle environ à  t=**5s**.

 >On conclut, que la première partie de la courbe correspond également, au moment où la goutte est accrochée à la paille et n'est pas encore entrée en contacte avec l'huile (ce que justifie son immobilisation) et donc n'a pas entamé son mouvement. Par la suite, la décroissance de la courbe correspond à une augmentation de la vitesse (on rappelle que l'axeY est descendant).Ensuite, on remarque que plus la goutte se rapproche de l'eau, plus sa vitesse diminue (phénomène que l'on esseyera d'expliquer plus tard). Pour finir, sa vitesse est nulle puisqu'elle est de nouveau immobile au contact de l'eau.


 **Graphique représentant l'accelération sur Y en fonction du temps.**


 ![trajectoireeeeee]

**Commentaire:**
 >La courbe débute à la position **0** du repère (choisi comme centre 0 de celui-ci au préalable).

 >Elle stagne de **0s** à **1.6s**. 
 
 >Puis la courbe est décroissante jusqu'à atteindre un minimum à t= **3s**.
 
 >Alors, celle-ci croit jusqu'à re-être nulle environ à  t=**5s**.

 >On conclut, que la première partie de la courbe correspond également, au moment où la goutte est accrochée à la paille et n'est pas encore entrée en contacte avec l'huile (ce que justifie son immobilisation) et donc n'a pas entamé son mouvement. Par la suite, la décroissance de la courbe correspond à une augmentation de l'accelération (on rappelle que l'axeY est descendant). Ensuite, on remarque que plus la goutte se rapproche de l'eau, plus son accelération diminue puisqu'en effet, sa vitesse diminue. Pour finir, son accelération est nulle puisqu'elle est de nouveau immobile au contact de l'eau.



## Conclusion

Suite à l'analyse de ces courbes obtenues, nous pouvons donc en tirer une conclusion qui est qu'elles sont en accord avec le mouvement réel filmé. 


 
 

