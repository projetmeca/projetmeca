# Introduction

Au fur et à mesure de ce projet, nous allons à la manière des scientifiques qui auront illuminé le monde et posé les bases de la mécanique ainsi que de ses problèmes, étudier un mouvement pour en déterminer et comprendre ses causes dans un objectif de déterminer dans une certaine mesure prévoir son futur. C'est ainsi que sur la lancée de nos illustres prédécesseurs ainsi qu'aux conseils que nous a fournis notre brillant professeur que nous avons choisi de travailler et d'étudier le mouvement d'une bulle d'eau dans un verre rempli d'huile. Nous étudions donc le système que représente notre bulle d'eau. Nous ferons 2 hypothèses, une première sans les forces de frottements et la poussée d'Archimède, puis une avec, et on pourra conclure sur lequel est le plus cohérent. Nous allons pour mener à bien ce projet, utiliser les nombreux chapitres que composent nos cours de mécaniques pour étudier ce mouvement, en s'appuyant notamment sur un modèle théorique base sur les différentes lois de Newton et d'énergétique et d'un modèle expérimental en se basant sur une expérience que l'on aura faîtes. Nous allons ainsi pouvoir calculer, comparer et conclure sur une approximation des deux résultats que nous obtiendront. C'est un mouvement intéressant qui nous aura pousser à faire d'autres expériences pour voir comment allait agir notre goutte d'eau avec un environnement  et des facteurs initiales différents. Nous allons donc ainsi présenter notre projet en 3 grande : 

Une partie d'expérimentation  dans laquelle nous allons faire l'expérience puis comprendre, et analyser ses données.

Une seconde où on se basera sur un modèle théorique

Et une troisième et dernière, qu'on abordera avec une optique de prévoir les valeurs d'une nouvelle expérience.
